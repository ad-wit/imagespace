<?php 

	function base_url($section = NULL) {
		return "http://localhost/imageSpace/$section";
	}

	function base_path($section = NULL) {
		return "imageSpace/$section";
	}

	function __autoload($classname) {
    	$filename = "./classes/". $classname .".php";
    	include_once($filename);
	}
?>