<?php
	session_start();
	require_once 'config.php';
	include 'include/header.php';
?>

	<form method="post" enctype="multipart/form-data" action="<?php echo base_url('upload/'); ?>" class="col-md-8">
		<div class="form-group">
			<label class="control-label">Upload a image</label>
			<input type="file" name="image[]" class="form-control" multiple="multiple">
		</div>
		<div>
			<input type="submit" value="Upload" name="submit">
		</div>
	</form>

<?php
	include 'include/footer.php';
?>