<?php
	
	require_once 'config.php';

	function rearrangeArray( $arr ) {
		foreach( $arr as $key => $all ) {
			foreach( $all as $i => $val ) {
				$new[$i][$key] = $val;
			}
		}
		return $new;
	}

	function random_str($length, $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ') {
	    $str = '';
	    $max = strlen($characters) - 1;
	    for ($i = 0; $i < $length; ++$i) {
	        $str .= $characters[rand(0, $max)];
	    }
	    return $str;
	}

	function generateId($filename = NULL) {

		htmlspecialchars($filename);
		$db = new Database();
		$id = array();

		do {

			$id["public"] = random_str(6);

			$oldPIds = $db->select('images', array('public_id'), "public_id = '{$id["public"]}'");

			if ( !$oldPIds['status'] ) {
				die();
			}

		} while ( $oldPIds['result'] != NULL );

		do {

			$id["delete"] = random_str(8);

			$oldDIds = $db->select('images', array('delete_id'), "delete_id = '{$id["delete"]}'");

			if ( !$oldDIds['status'] ) {
				die();
			}

		} while ( $oldDIds['result'] != NULL );

		return $id;
	}

	function insertData($publicId, $deleteId, $type) {

		$db = new Database();

		$values = array(
			'public_id' => $publicId,
			'image_type' => $type,
			'delete_id' => $deleteId
		);
		$res = $db->insert($values, 'images');

		return $res;
	}

	function checkFiles( $images ) {

		$types = array('jpg', 'jpeg', 'png', 'gif');

		foreach ($images as $key => $image) {

			$extension = pathinfo($image["name"], PATHINFO_EXTENSION);
			$userError = array();

			if ( $image["size"] == 0 ) {
				
				$userError["noFile"] = "No image found.";

			} else if ( !in_array($extension, $types) ) {

				$userError["extension"] = "Image type not allowed.";

			} else if ( $image["size"] > 7340032 ) {

				$userError["size"] = "Image size too large.";
			}

			$image["userError"] = $userError;
			$images[$key] = $image;
		}

		return $images;
	}

	function uploadImages( $images ) {

		$results = array();
		
		foreach ($images as $key => $image) {
		
			if ( $image["userError"] == NULL && $image["error"] == NULL ) {

				$extension = pathinfo($image["name"], PATHINFO_EXTENSION);
				$id = generateId($image['name']);
				$publicId = $id["public"];
				$target_path = "./image/{$id["public"]}.$extension";

				if ( move_uploaded_file($image['tmp_name'], $target_path) ) {
					if ( insertData($id["public"], $id["delete"], $extension) ) {

						$results[$key] = array(
											"error" 	   => FALSE,
											"server_error" => FALSE,
											"name" 		   => $image["name"],
											"image" 	   => $id["public"].$extension,
											"delete_id"    => $image["delete"]
										);
					}
				} else {

						$results[$key] = array(
											"error" 	   => FALSE,
											"server_error" => TRUE,
											"name" 		   => NULL,
											"image" 	   => NULL,
											"delete_id"    => NULL
										);	

				}

			} else {

				$results[$key] = array(
									"error" 	   => $image["userError"],
									"server_error" => FALSE,
									"name" 		   => $image["name"],
									"image" 	   => NULL,
									"delete_id"    => NULL
								);

			}

		}

		session_start();

		$_SESSION['results'] = $results;
	}

	if ( $_SERVER["REQUEST_METHOD"] == "POST" ) {

		$images = rearrangeArray( $_FILES["image"] );

		$images = checkFiles( $images );

		uploadImages( $images );

		header('Location:'.base_url("links.php"));
		exit();
		
	}
?>