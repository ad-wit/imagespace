<?php
	require_once '../config.php';
	include '../include/header.php';
?>

	<form method="post" action="<?php echo base_url('admin/login/'); ?>" class="col-md-4">
		<div class="form-group">
			<label class="control-label">Username</label>
			<input type="text" name="username" class="form-control">
		</div>
		<div class="form-group">
			<label class="control-label">Password</label>
			<input type="password" name="password" class="form-control">
		</div>
		<div>
			<input type="submit" value="Login" name="admin_login">
		</div>
	</form>

<?php
	include '../include/footer.php';
?>