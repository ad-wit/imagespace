<?php
	require_once 'config.php';
	include 'include/header.php';

	if ( isset($_GET["id"]) ) {
		
		$filename = htmlspecialchars($_GET["id"]);
		$db = new Database();
		$result = $db->select('images', array('*'), "delete_id = '$filename' AND is_deleted = 0");

		if ( $result["result"] == NULL ) {
			header('Location:'.base_url());
			exit();
		}

		$image = $result["result"][0];

		?>
			<img src="<?php echo base_url("image/{$image["public_id"]}.{$image["image_type"]}"); ?>" alt="No Image" id="delete-image">
			<p>Are you sure to delete this image, you will never recover your image.
			<a href="<?php echo base_url("deleteImage/{$image["delete_id"]}"); ?>" class="btn btn-primary btn-sm">Delete</a>
			<a href="<?php echo base_url();?>" class="btn btn-danger btn-sm">Cancel</a></p>
		<?php 
	} else if ( isset($_GET["status"]) ) {

		if ( $_GET["status"] == "deleted" ) {

			echo "Your image is deleted.";
		}
	} else {
		header('Location:'.base_url());
		exit();
	}

	include 'include/footer.php';
?>