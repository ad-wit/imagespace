<?php
	
	require_once 'config.php';
	include 'include/header.php';

	// print_r($results);
	if ( isset($_SESSION['results']) ) {
		
		$results = $_SESSION['results'];
		foreach ( $results as $key => $image ) {
			if ( $image["error"] == FALSE ) {
				if ( $image["server_error"] == FALSE ) {

					?>

						<div>
							<p>Links for image <?php echo $image["name"]; ?></p>
							<p>Link to image is <a href="<?php echo base_url("image/".$image["image"]); ?>"><?php base_url("image/".$image["image"]); ?></a></p>
							<p>Link to delete image is <a href="<?php echo base_url("delete/".$image["delete"]); ?>"><?php echo base_url("delete/".$image["delete"]); ?></a></p>
							<img src="<?php echo base_url("image/".$image["image"]); ?>" alt="Your Image" id="show-image">
						</div>

					<?php
				} else {

					?>

						<div>
							<p>Some error occured while uploading. Please try again.</p>
						</div>	

					<?php
				}
			} else {

				$error = $image["error"];
				?>

					<div style="background-color:#ffa5a5;">
						<p>Error in image <?php echo $image["name"]; ?></p>
						<p>
							<?php 
								foreach ($error as $value) {
									echo $value."<br>";
								}
							?>
						</p>
					</div>

				<?php

			}
		}

		session_destroy();

	} else {
		
		// header('Location:'.base_url());
		// exit();
	}
	

	include 'include/footer.php';
?>