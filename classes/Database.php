<?php

/**
* Database operations
*/
class Database {
	
	private $server = "localhost";
	private $username = "root";
	private $password = "adwait@30";
	private $dbname = "imagespace";
	private $pdo;

	function __construct() {

		try {
			   	$conn = new PDO("mysql:host=$this->server;dbname=$this->dbname", $this->username, $this->password);
				// set the PDO error mode to exception
				$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			}
		catch(PDOException $e)
			{
				die ("Connection failed: " . $e->getMessage());
			}

		$this->pdo = $conn;
	}

	public function disconnect() {

		$this->pdo->close();
	}

	public function select( $from = NULL, $attr = array('*'), $where = NULL, $order = "ASC", $by = NULL ) {

		$result = array( "status" => NULL, "reason" => NULL, "result" => NULL );
		$sql = NULL;

		if ( !is_array($attr) ) {
			
			$result["status"] = FALSE;
			$result["reason"] = $result["reason"] . "The \$attr parameter passed is not an array.";
		}

		if ( $from == NULL ) {

			$result["status"] = FALSE;
			$result["reason"] = $result["reason"] . "The \$from parameter is not passed.";
		}

		if ( $order != "DESC" || $order != "ASC" ) {
			
			$result["status"] = FALSE;
			$result["reason"] = $result["reason"] . "The \$order parameter passed is invalid.";
		}

		if ( $result["status"] == NULL ) {
			
			$sql = "SELECT " . implode(", ", $attr) . " FROM $from ";

			if ( is_array($where) ) {
				$i = 1;
				foreach ($where as $key => $value) {
					$i++;
					$sql = $sql . "$key = $value ";
					if ( $i < count($where) ) {
						
						$sql = $sql . "AND ";
					}
				}
			} else if ( $where != NULL ) {

				$sql = $sql."WHERE $where ";
			}

			if ( $by != NULL ) {
				
				$sql = $sql . "ORDER BY $by ";
				$sql = $sql . "$order";
			}

			$stmt = $this->pdo->query($sql);

			if ( $stmt ) {
				
				$result["status"] = TRUE;
				$result["reason"] = NULL;
				$result["result"] = $stmt->fetchAll(PDO::FETCH_ASSOC);	
			} else {

				$result["status"] = FALSE;
				$result["reason"] = "Some server error occured.";
				$result["result"] = NULL;
			}
		}

		return $result;
	}

	public function insert( $values, $into ) {

		$result = array( "status" => NULL, "reason" => NULL, "result" => NULL );
		$sql = NULL;

		if ( !is_array($values) ) {
			
			$result["status"] = FALSE;
			$result["reason"] = $result["reason"] . "The \$values parameter passed is not an array.";
		}

		if ( $result["status"] == NULL  ) {
			
			$sql = "INSERT INTO $into (" . implode(", ", array_keys($values)) . ") VALUES ('" . implode("', '",  array_values($values)) . "')";

			$stmt = $this->pdo->query($sql);

			if ( $stmt ) {
				
				$result["status"] = TRUE;
				$result["reason"] = NULL;
				$result["result"] = "Successful";
			} else {

				$result["status"] = FALSE;
				$result["reason"] = "Some server error occured.";
				$result["result"] = "Unsucessful";
			}
		}

		return $result;
	}

	public function update( $table = NULL, $values = NULL, $where = NULL ) {

		$result = array( "status" => NULL, "reason" => NULL, "result" => NULL );
		$sql = NULL;
		
		if ( $table == NULL ) {
			
			$result["status"] = FALSE;
			$result["reason"] = $result["reason"] . "The \$table parameter is not passed.";
		}

		if ( !is_array($values) ) {
			
			$result["status"] = FALSE;
			$result["reason"] = $result["reason"] . "The \$values parameter passed is not an array.";
		}

		if ( $result["status"] == NULL ) {
			
			$sql = "UPDATE $table SET ";

			$i = 1;
			foreach ($values as $key => $value) {
				$i++;
				$sql = $sql . "$key=$value ";
				if ( $i < count($values) ) {
					
					$sql = $sql . ", ";
				}
			}

			if ( is_array($where) ) {
				$i = 1;
				foreach ($where as $key => $value) {
					$i++;
					$sql = $sql . "$key = $value ";
					if ( $i < count($where) ) {
						
						$sql = $sql . "AND ";
					}
				}
			} else if ( $where != NULL ) {

				$sql = $sql."WHERE $where ";
			}

			$stmt = $this->pdo->query($sql);

			if ( $stmt ) {
				
				$result["status"] = TRUE;
				$result["reason"] = NULL;
				$result["result"] = "Updated";	
			} else {

				$result["status"] = FALSE;
				$result["reason"] = "Some server error occured.";
				$result["result"] = NULL;
			}
		}

		return $result;
	}

}