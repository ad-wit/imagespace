<!DOCTYPE html>
<html>
<head>
	<title>asd</title>
</head>
<body>
<form method="post" action="<?php echo base_url('home/add'); ?>" id="modal_addform" class="form-horizontal">
						<div id="result"></div>
						<fieldset>
							<div class="control-group">
								<label class="control-label" for="add_owner_name">Your Name <span class="require">&#42;</span></label>
								<div class="controls">
									<input type="text" class="input-xlarge form-control" name="owner_name" id="add_owner_name"  maxlength="100" required>
									<span class="help-block error" data-name="owner_name"></span>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label" for="add_owner_email">Your Email <span class="require">&#42;</span></label>
								<div class="controls">
									<input type="email" class="input-xlarge form-control" name="owner_email" id="add_owner_email"  maxlength="100" required>
									<span class="help-block error" data-name="owner_email"></span>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label" for="add_title">Company Name <span class="require">&#42;</span></label>
								<div class="controls">
										<input type="text" class="input-xlarge form-control" name="company_title" id="add_title"  maxlength="100" autocomplete="off" required>
										<span class="help-block error" data-name="company_title"></span>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label" for="add_type">Company Type <span class="require">&#42;</span></label>
								<div class="controls">
									<select name="company_type" id="add_type" class="input-xlarge form-control">
										<option value="startup">Startup</option>
										<option value="accelerator">Accelerator</option>
										<option value="incubator">Incubator</option>
										<option value="coworking">Coworking</option>
										<option value="investor">VC/Angel</option>
										<option value="service">Consulting Firm</option>
										<option value="hackerspace">Hackerspace</option>
										<option value="hangout">Hangout</option>
									</select>
									<span class="help-block error" data-name="company_type"></span>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label" for="add_address">Address <span class="require">&#42;</span></label>
								<div class="controls">
									<!-- <input type="text" class="input-xlarge form-control" name="address" id="add_address"> -->
									<textarea name="company_address" rows="3" cols="80" class="input-xlarge form-control" id="add_address"></textarea>
									<span class="help-block error" data-name="company_address"></span>
									<p class="help-block">
										Should be your <b>full street address (including city and pincode)</b>.
										If it works on Google Maps, it will work here.
									</p>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label" for="add_uri">Website URL <span class="require">&#42;</span></label>
								<div class="controls">
									<input type="url" class="input-xlarge form-control" id="add_uri" name="company_uri" placeholder="http://">
									<span class="help-block error" data-name="company_uri"></span>
									<p class="help-block">
										Should be your full URL with no trailing slash, e.g. "http://www.yoursite.com"
									</p>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label" for="add_description">Description</label>
								<div class="controls">
									<!-- <input type="text" class="input-xlarge form-control" id="add_description" name="description"  maxlength="150"> -->
									<textarea name="description" rows="3" cols="80" class="input-xlarge form-control" id="add_description" maxlength="150"></textarea>
									<span class="help-block error" data-name="description"></span>
									<p class="help-block">
										Brief, concise description. What's your product? What problem do you solve? Keep it simple. Max 150 chars.
									</p>
								</div>
							</div>
							<input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" />
						</fieldset>
					<div class="modal-footer">
						<button type="submit" class="btn btn-primary">Submit for Review</button>
						<a href="#" class="btn" data-dismiss="modal" style="float: right;">Close</a>
					</div>
					</form>
</body>
</html>