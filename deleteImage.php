<?php
	require_once 'config.php';
	include 'include/header.php';

	if ( isset($_GET["id"]) ) {

		$filename = htmlspecialchars($_GET["id"]);
		$db = new Database();
		$result = $db->select('images', array('*'), "delete_id = '$filename' AND is_deleted = 0");

		if ( $result["result"] == NULL ) {
			header('Location:'.base_url());
			exit();
		}

		$image = $result["result"][0];

		if ( rename("./image/{$image['public_id']}.{$image['image_type']}", "./deletedImages/{$image['delete_id']}.{$image['image_type']}") ) {

			$res = $db->update('images', array('is_deleted'=>1), "delete_id = '{$image['delete_id']}'");

			if ( $res["result"] == "Updated" ) {
				
				header('Location:'.base_url("delete.php?status=deleted"));
			} else {
				rename("./deletedImages/{$image['delete_id']}.{$image['image_type']}", "./image/{$image['public_id']}.{$image['image_type']}");
			}
		}

	} else {
		header('Location: '.base_url());
		exit();
	}

	include 'include/footer.php';
?>